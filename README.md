# CSA Homeworks

# HW 2.
### Создан terraform конфиг для создания ресурсов в AWS. В качестве backend использован terraform  cloud.
  - Создаются
    - 2 бакета: freecontent-otus, paidcontent-otus
    - 2 пользователя: freeuser, paiduser (Необходимо через веб-консоль администратора задать пароль и разрешить вход в веб-консоль для пользователей)
    - 2 группы: freeusers, paidusers. Пользователь freeuser добавляется только в группу freeusers. Пользователь paiduser добавляется в обе группы.
    - 2 политики: paidcontent-readonly (позвояет читать данные из бакетов paidcontent и freecontent), freecontent-readonly (позвояет читать данные только из бакета freecontent)

### Запуск
```bash
# Планирование
terraform plan
# Создание ресурсов
terraform apply
# Удаление созданных ресурсов
terraform destroy
```
### Проверка
![00](images/hw2/00.png)
![01](images/hw2/01.png)
![02](images/hw2/02.png)
![03](images/hw2/03.png)
![04](images/hw2/04.png)
![05](images/hw2/05.png)
![06](images/hw2/06.png)
![07](images/hw2/07.png)