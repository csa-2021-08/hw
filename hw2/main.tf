provider "aws" {
  profile = "default"
  region  = "eu-central-1"
}

resource "aws_s3_bucket" "freecontent_bucket" {
  bucket        = "freecontent-otus"
  acl           = "private"
  force_destroy = "true"

  tags = {
    Name        = "Freecontent bucket"
    Environment = "Dev"
    Project     = "Otus"
  }
}

resource "aws_s3_bucket" "paidcontent_bucket" {
  bucket = "paidcontent-otus"
  acl    = "private"

  force_destroy = "true"
  tags = {
    Name        = "Paidcontent bucket"
    Environment = "Dev"
    Project     = "Otus"
  }
}

resource "aws_iam_user" "freeuser" {
  name          = "freeuser"
  force_destroy = "true"
}

resource "aws_iam_user" "paiduser" {
  name          = "paiduser"
  force_destroy = "true"
}

resource "aws_iam_group" "freeusers" {
  name = "freeusers"
}

resource "aws_iam_group" "paidusers" {
  name = "paidusers"
}

resource "aws_iam_user_group_membership" "freeusers" {
  user = aws_iam_user.freeuser.name

  groups = [
    aws_iam_group.freeusers.name,
  ]
}

resource "aws_iam_user_group_membership" "paidusers" {
  user = aws_iam_user.paiduser.name

  groups = [
    aws_iam_group.freeusers.name,
    aws_iam_group.paidusers.name,
  ]
}

resource "aws_iam_group_policy" "s3-freecontent-readonly" {
  name  = "s3-freecontent-readonly"
  group = aws_iam_group.freeusers.name

  policy = <<POLICY
{
   "Version":"2012-10-17",
   "Statement":[
      {
         "Action":[
            "s3:ListAllMyBuckets",
            "s3:GetBucketLocation"
         ],
         "Effect":"Allow",
         "Resource":[
            "*"
         ]
      },
      {
         "Action":[
            "s3:ListBucket"
         ],
         "Effect":"Allow",
         "Resource":[
            "${aws_s3_bucket.freecontent_bucket.arn}"
         ]
      },
      {
         "Action":[
            "s3:GetObject"
         ],
         "Effect":"Allow",
         "Resource":[
            "${aws_s3_bucket.freecontent_bucket.arn}/*"
         ]
      }
   ]
}
POLICY
}

resource "aws_iam_group_policy" "paidcontent-readonly" {
  name  = "s3-paidcontent-readonly"
  group = aws_iam_group.paidusers.name

  policy = <<POLICY
{
   "Version":"2012-10-17",
   "Statement":[
      {
         "Action":[
            "s3:ListAllMyBuckets",
            "s3:GetBucketLocation"
         ],
         "Effect":"Allow",
         "Resource":[
            "*"
         ]
      },
      {
         "Action":[
            "s3:ListBucket"
         ],
         "Effect":"Allow",
         "Resource":[
            "${aws_s3_bucket.freecontent_bucket.arn}",
            "${aws_s3_bucket.paidcontent_bucket.arn}"
         ]
      },
      {
         "Action":[
            "s3:GetObject"
         ],
         "Effect":"Allow",
         "Resource":[
            "${aws_s3_bucket.freecontent_bucket.arn}/*",
            "${aws_s3_bucket.paidcontent_bucket.arn}/*"
         ]
      }
   ]
}
POLICY
}

