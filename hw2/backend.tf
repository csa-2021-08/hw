terraform {
  backend "remote" {
    organization = "Otus-CSA"

    workspaces {
      name = "dev"
    }
  }
}